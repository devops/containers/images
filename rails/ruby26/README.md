# Rails/Ruby 2.6 application builder image

Build the builder image:

    $ docker build -t rails .

Test the builder image:

    $ IMAGE_NAME=rails test/run

Use the builder image:

    $ s2i build <project-src> rails <project-img-tag>

See `s2i build --help` for more information and options.
