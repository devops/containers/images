# Apache HTTPD 2.4 container base image with Shibboleth support

This image provides Apache HTTPD 2.4 server on ports 80 and 443 running in the foreground.
mod_shib is installed and loaded by default to support a Shibboleth service provider.
