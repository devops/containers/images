#!/bin/bash

if grep -q "__SHIB_ENTITY_ID__" /etc/shibboleth/shibboleth2.xml; then
    if [ -z "${SHIB_ENTITY_ID}" ]; then
	echo "SHIB_ENTITY_ID variable is not set, cannot continue."
	exit 1
    else
	sed -i -e 's|__SHIB_ENTITY_ID__|'"${SHIB_ENTITY_ID}"'|' /etc/shibboleth/shibboleth2.xml
    fi

    if [ ! -e /run/secrets/shib_cert ]; then
	echo "Cannot find Shib SP certificate."
	exit 1
    fi

    if [ ! -e /run/secrets/shib_key ]; then
	echo "Cannot find Shib SP private key."
	exit 1
    fi
fi

exec "$@"
