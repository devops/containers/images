# Shibboleth Service Provider (SP) container image for Duke University

This image is based on CentOS 7 and Shibboleth 3.x.

A customized attribute map and SP config (shibboleth2.xml) are provided
for convenience. The config has three requirements:

- `SHIB_ENTITY_ID` variable must be set.
- The TLS certificate must be stored in the secret `shib_cert`.
- The TLS private key must be stored in the secret `shib_key`.

Of course, a custom config may also be provided at run time,
e.g., via a bind mount.
